#autoRulePrincUser.py
import os
import re

def main():
  outFile = open("ruleSample.txt", "w")

  insert2 = '  '
  insert4 = insert2 + insert2
  insert8 = insert4 + insert4
  insert10 = insert8 + insert2
  ruleHeader1 = 'profile autoGenRule\n{\n'
  ruleHeader2 = 'meta:\n'
  ruleHeader3 = 'author = "Autogen Script"\n'

  ruleHeader = ruleHeader1 + insert2 + ruleHeader2 + insert4 + ruleHeader3 + '\n'
  outFile.write(ruleHeader)


  functionHeader1 = 'function:\n'
  functionHeader2 = 'func userPrincipalEval ()\n'

  functionHeader = insert2 + functionHeader1 + insert4 + functionHeader2

  functionEval1 = 'if (strings.to_lower(udm.principal.user.userid)) == '
  functionEval1a = '(strings.to_lower(udm.principal.user.userid)) == '
  functionEval2 = '"'
  functionEval3 = '"\n'
  functionEval4 = 'or '
  outFile.write(functionHeader)

  outputLine = functionEval1 + functionEval2
  outFile.write(insert4)
  outFile.write(insert4)
  outFile.write(outputLine)

  #if (strings.to_lower(udm.principal.user.userid)) == fileValue
  #eval1 + readline
  #or (strings.to_lower(udm.principal.user.userid)) == fileValue2
  #eval4 + eval1a + eval2 + readline + eval2

#set up regex

  #open source file, sample.txt
  try:
    with open("sample.txt", "r") as inFile:
      user_id = inFile.readline()
      user_id = user_id.lower().rstrip("\n")

      outputLine= user_id + functionEval3
      outFile.write(outputLine)
      lines = inFile.readlines()
      for user_id in lines:
        user_id = user_id.lower().rstrip("\n")
        outputLine = insert10 + functionEval4 + functionEval1a + functionEval2 + user_id + functionEval2 + '\n'
        outFile.write(outputLine)

    funcClose1 = insert8 +'then\n' + insert10 + 'return true\n' + insert8 + 'end\n' + insert8 + 'return false\n' + insert4 + 'end\n'
    outFile.write(funcClose1)

    #condition:
    #if userPrincipalEval() then
    #outcome.match()
    #end
    #}
    cond1 = 'condition:\n'
    cond2 = 'if userPrincipalEval() then\n'
    cond3 = 'outcome.match()\n'
    cond4 = 'end\n'
    cond5 = '}\n'

    outCond = '\n\n' + insert2 + cond1 + insert4 + cond2 + insert8 +cond3 + insert4 + cond4 + cond5
    outFile.write(outCond)
  except FileNotFoundError:
    print("File not found. Try again")
  else:
    inFile.close()
    outFile.close()
  finally:
    print("Program is done")
  

if __name__ == '__main__':
  main()
