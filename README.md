# Rule Creation - Principal User List

Automate rule generation based on a list of values to evaluate against Chronicle principal.user.userid field.  

## Current values hardcoded

* Input file: sample.txt
* Output file: ruleSample.txt

## Known issues
* No handling of arguments

